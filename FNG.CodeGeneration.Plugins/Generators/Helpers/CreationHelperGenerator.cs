﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;

namespace FNG.CodeGeneration.Plugins
{
    public class CreationHelperGenerator : AbstractGenerator
    {
        public override string name => "Creation Helper";

        private const string FLAG_TEMPLATE = @"public static class ${Context}${Component}CreationHelpers
{
    public static ${Context}Entity Create${Component}(this ${Context}Context c)
    {
        var e = c.CreateEntity();
        e.is${Component} = true;
        return e;
    }
}
";

        private const string STANDARD_TEMPLATE = @"public static class ${Context}${Component}CreationHelpers
{
    public static ${Context}Entity Create${Component}(this ${Context}Context c, ${TypedArgs})
    {
        var e = c.CreateEntity();
        e.Replace${Component}(${MethodArgs});
        return e;
    }
}
";

        private const string TYPED_METHOD_ARGS = @"${FieldType} ${fieldName}";
        private const string METHOD_ARGS = @"${fieldName}";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data) => data
            .OfType<CreationData>().SelectMany(generate).ToArray();

        private CodeGenFile[] generate(CreationData data)
        {
            var path = "CreationHelpers" + Path.DirectorySeparatorChar;
            var componentName = data.GetTypeName().ToComponentName(true);
            var contextNames = data.GetContextNames();
            var memberData = data.GetMemberData();
            var template = memberData.Length == 0 ? FLAG_TEMPLATE : STANDARD_TEMPLATE;

            var result = new List<CodeGenFile>();
            foreach (var contextName in contextNames)
            {
                var fileContents = template.Replace("${Component}", componentName).Replace("${Context}", contextName)
                    .Replace("${TypedArgs}", getMethodArgs(memberData, true))
                    .Replace("${MethodArgs}", getMethodArgs(memberData, false));

                var fileName = path + contextName + componentName + "CreationHelpers.cs";
                result.Add(new CodeGenFile(fileName, fileContents, GetType().FullName));
            }

            return result.ToArray();
        }

        private static string getMethodArgs(MemberData[] data, bool typed)
        {
            var template = typed ? TYPED_METHOD_ARGS : METHOD_ARGS;
            return string.Join(", ",
                data.Select(member
                    => template.Replace("${FieldType}", member.type)
                        .Replace("${fieldName}", member.name.LowercaseFirst())).ToArray());
        }
    }
}