﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;

namespace FNG.CodeGeneration.Plugins
{
    public class CollectionHelperGenerator : AbstractGenerator
    {
        public override string name => "Collection Helpers";

        private const string TEMPLATE = @"public static class ${Context}${Component}CollectionHelpers
{
    public static void Add${Component}Item(this ${Context}Entity e, ${ItemType} item)
    {
        if (!e.has${Component}) e.Add${Component}(new ${CollectionType}() {item});
        else if (!e.${component}.${fieldName}.Contains(item))
        {
            e.${component}.${fieldName}.Add(item);
            e.Replace${Component}(e.${component}.${fieldName});
        }
    }

    public static void Remove${Component}Item(this ${Context}Entity e, ${ItemType} item, bool removeComponentIfCollectionEmpty = true)
    {
        if (!e.has${Component}) return;
        var removed = e.${component}.${fieldName}.Remove(item);
        if (removeComponentIfCollectionEmpty && e.${component}.${fieldName}.Count == 0) e.Remove${Component}();
        else if (removed) e.Replace${Component}(e.${component}.${fieldName});
    }

    public static void Clear${Component}Collection(this ${Context}Entity e)
    {
        if (!e.has${Component}) return;
        e.${component}.${fieldName}.Clear();
        e.Replace${Component}(e.${component}.${fieldName});
    }
    
    public static ${CollectionType} GetOrCreate${Component}Collection(this ${Context}Entity e)
        => e.has${Component} ? e.${component}.${fieldName} : new ${CollectionType}();

    public static bool Has${Component}Item(this ${Context}Entity e, ${ItemType} item)
        => e.has${Component} && e.${component}.${fieldName}.Contains(item);
}
";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data) => data
            .OfType<CollectionData>().SelectMany(generate).ToArray();

        private CodeGenFile[] generate(CollectionData data)
        {
            var path = "CollectionHelpers" + Path.DirectorySeparatorChar;
            var componentName = data.GetTypeName().ToComponentName(true);
            var componentNameLowercaseFirst = componentName.LowercaseFirst();
            var contextNames = data.GetContextNames();
            var collectionType = data.GetMemberData()[0].type;
            var fieldName = data.GetMemberData()[0].name;
            var itemType = GetStringBetweenCharacters(collectionType, '<', '>');

            var result = new List<CodeGenFile>();
            foreach (var contextName in contextNames)
            {
                var fileContents = TEMPLATE.Replace("${Component}", componentName).Replace("${Context}", contextName)
                    .Replace("${ItemType}", itemType).Replace("${CollectionType}", collectionType)
                    .Replace("${component}", componentNameLowercaseFirst).Replace("${fieldName}", fieldName);

                var fileName = path + contextName + componentName + "CollectionHelpers.cs";
                result.Add(new CodeGenFile(fileName, fileContents, GetType().FullName));
            }

            return result.ToArray();
        }

        private static string GetStringBetweenCharacters(string input, char charFrom, char charTo)
        {
            var posFrom = input.IndexOf(charFrom);
            if (posFrom == -1) return string.Empty;
            var posTo = input.IndexOf(charTo, posFrom + 1);
            return posTo != -1 ? input.Substring(posFrom + 1, posTo - posFrom - 1) : string.Empty;
        }
    }
}