﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;

namespace FNG.CodeGeneration.Plugins
{
    public class EnumHelperGenerator : AbstractGenerator
    {
        public override string name => "Enum Helper";

        private const string TEMPLATE = @"public static class ${Context}${Component}EnumHelpers
{
    public static bool Is${Component}(this ${Context}Entity e, ${EnumType} item)
        => e.has${Component} && e.${component}.${fieldName} == item;
}
";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data) => data
            .OfType<EnumData>().SelectMany(generate).ToArray();

        private CodeGenFile[] generate(EnumData data)
        {
            var path = "EnumHelpers" + Path.DirectorySeparatorChar;
            var componentName = data.GetTypeName().ToComponentName(true);
            var componentNameLowercaseFirst = componentName.LowercaseFirst();
            var contextNames = data.GetContextNames();
            var enumType = data.GetMemberData()[0].type;
            var fieldName = data.GetMemberData()[0].name;

            var result = new List<CodeGenFile>();
            foreach (var contextName in contextNames)
            {
                var fileContents = TEMPLATE.Replace("${Component}", componentName).Replace("${Context}", contextName)
                    .Replace("${EnumType}", enumType).Replace("${component}", componentNameLowercaseFirst)
                    .Replace("${fieldName}", fieldName);

                var fileName = path + contextName + componentName + "EnumHelpers.cs";
                result.Add(new CodeGenFile(fileName, fileContents, GetType().FullName));
            }

            return result.ToArray();
        }
    }
}