﻿using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using Entitas.CodeGeneration.Attributes;
using Entitas.CodeGeneration.Plugins;

namespace FNG.CodeGeneration.Plugins
{
    public class ReactiveEventSystemGenerator : AbstractGenerator
    {
        public override string name => "Reactive Event System";

        private const string ANY_TARGET_TEMPLATE =
@"public sealed class Reactive${Event}EventSystem : Entitas.ReactiveSystem<${EntityType}> 
{
    readonly Entitas.IGroup<${EntityType}> _listeners;
    readonly System.Collections.Generic.List<${EntityType}> _entityBuffer;
    readonly System.Collections.Generic.List<I${EventListener}> _listenerBuffer;
    readonly Entitas.IGroup<${EntityType}> _sourceGroup;
    private readonly System.Collections.Generic.List<${EntityType}> _sourceEntities;

    public Reactive${Event}EventSystem(Contexts contexts) : base(contexts.${contextName}) 
    {
        _listeners = contexts.${contextName}.GetGroup(${MatcherType}.${EventListener});
        _entityBuffer = new System.Collections.Generic.List<${EntityType}>();
        _listenerBuffer = new System.Collections.Generic.List<I${EventListener}>();
        _sourceGroup = contexts.game.GetGroup(${MatcherType}.${ComponentName});
        _sourceEntities = new System.Collections.Generic.List<${EntityType}>();
    }

    protected override Entitas.ICollector<${EntityType}> GetTrigger(Entitas.IContext<${EntityType}> context) 
    {
        return Entitas.CollectorContextExtension.CreateCollector(
            context, Entitas.TriggerOnEventMatcherExtension.${GroupEvent}(${MatcherType}.${EventMatcher})
        );
    }

    protected override bool Filter(${EntityType} entity) 
    {
        return ${filter};
    }

    protected override void Execute(System.Collections.Generic.List<${EntityType}> entities) 
    {        
        _sourceGroup.GetEntities(_sourceEntities);
        foreach (var listenerEntity in entities)
        {
            _listenerBuffer.Clear();
            _listenerBuffer.AddRange(listenerEntity.${eventListener}.value);            
            foreach (var e in _sourceEntities) 
            {
                ${cachedAccess}
                foreach (var listener in _listenerBuffer) 
                {
                    listener.On${EventComponentName}${EventType}(e${methodArgs});
                }
            }
        }
    }
}
";

        private const string SELF_TARGET_TEMPLATE =
@"public sealed class Reactive${Event}EventSystem : Entitas.ReactiveSystem<${EntityType}> 
{
    readonly System.Collections.Generic.List<I${EventListener}> _listenerBuffer;

    public Reactive${Event}EventSystem(Contexts contexts) : base(contexts.${contextName}) 
    {
        _listenerBuffer = new System.Collections.Generic.List<I${EventListener}>();
    }

    protected override Entitas.ICollector<${EntityType}> GetTrigger(Entitas.IContext<${EntityType}> context) 
    {
        return Entitas.CollectorContextExtension.CreateCollector(
            context, Entitas.TriggerOnEventMatcherExtension.${GroupEvent}(${MatcherType}.${EventMatcher})
        );
    }

    protected override bool Filter(${EntityType} entity)
    {
        return ${filter};
    }

    protected override void Execute(System.Collections.Generic.List<${EntityType}> entities) 
    {
        foreach (var e in entities) 
        {
            ${cachedAccess}
            _listenerBuffer.Clear();
            _listenerBuffer.AddRange(e.${eventListener}.value);
            foreach (var listener in _listenerBuffer) 
            {
                listener.On${ComponentName}${EventType}(e${methodArgs});
            }
        }
    }
}
";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
        {
            return data.OfType<ComponentData>().Where(d => d.IsEvent()).SelectMany(generate).ToArray();
        }

        private CodeGenFile[] generate(ComponentData data)
        {
            return data.GetContextNames().SelectMany(contextName => generate(contextName, data)).ToArray();
        }

        private CodeGenFile[] generate(string contextName, ComponentData data)
        {
            return data.GetEventData().Select(eventData =>
            {
                var methodArgs = data.GetEventMethodArgs(eventData,
                    ", " + (data.GetMemberData().Length == 0
                        ? data.PrefixedComponentName()
                        : getMethodArgs(data.GetMemberData())));

                var cachedAccess = data.GetMemberData().Length == 0
                    ? string.Empty
                    : "var component = e." + data.ComponentNameValidLowercaseFirst() + ";";

                if (eventData.eventType == EventType.Removed)
                {
                    methodArgs = string.Empty;
                    cachedAccess = string.Empty;
                }

                var template = eventData.eventTarget == EventTarget.Self ? SELF_TARGET_TEMPLATE : ANY_TARGET_TEMPLATE;

                var fileContent = template.Replace("${GroupEvent}", "Added") // eventData.eventType.ToString())
                    .Replace("${filter}", getFilter(data, contextName, eventData))
                    .Replace("${cachedAccess}", cachedAccess).Replace("${methodArgs}", methodArgs)
                    .Replace("${EventMatcher}", data.EventListener(contextName, eventData))
                    .Replace(data, contextName, eventData);

                return new CodeGenFile(
                    "ReactiveEvents" + Path.DirectorySeparatorChar + "Systems" + Path.DirectorySeparatorChar +
                    "Reactive" + data.Event(contextName, eventData) + "EventSystem.cs", fileContent,
                    GetType().FullName);
            }).ToArray();
        }

        private string getFilter(ComponentData data, string contextName, EventData eventData)
        {
            var filter = string.Empty;

            if (eventData.eventTarget == EventTarget.Any)
            {
                filter = "entity.has" + data.EventListener(contextName, eventData);
                return filter;
            }

            if (data.GetMemberData().Length == 0)
            {
                switch (eventData.eventType)
                {
                    case EventType.Added:
                        filter = "entity." + data.PrefixedComponentName();
                        break;
                    case EventType.Removed:
                        filter = "!entity." + data.PrefixedComponentName();
                        break;
                }
            }
            else
            {
                switch (eventData.eventType)
                {
                    case EventType.Added:
                        filter = "entity.has" + data.ComponentName();
                        break;
                    case EventType.Removed:
                        filter = "!entity.has" + data.ComponentName();
                        break;
                }
            }

            if (eventData.eventTarget == EventTarget.Self)
                filter += " && entity.has" + data.EventListener(contextName, eventData);

            return filter;
        }

        private string getMethodArgs(MemberData[] memberData)
        {
            return string.Join(", ", memberData.Select(info => "component." + info.name).ToArray());
        }
    }
}