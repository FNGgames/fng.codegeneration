﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class EntityConfigInterfaceGenerator : ConfigGenerator
    {
        public override string name => "Entity Config (Interface)";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Entity, DestinationType.Interface, data);
    }
}