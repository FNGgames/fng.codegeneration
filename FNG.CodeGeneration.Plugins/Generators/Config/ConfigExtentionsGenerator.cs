﻿using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;

namespace FNG.CodeGeneration.Plugins
{
    public class ConfigExtentionsGenerator : AbstractGenerator
    {
        public override string name => "Config Extentions";

        private const string TEMPLATE = @"public static class ConfigExtentions
{
${ContextsConfigMethod}

${PerContextConfigMethods}

${PerEntityConfigMethods}
}"; 

        private const string CONTEXTS_CONFIG_METHOD = 
@"    public static void Configure(this Contexts contexts, IContextsConfig config)
    {
${ContextMethodCalls}
    }";
        
        private const string PER_CONTEXT_CONFIG_METHOD =
@"    public static void Configure(this ${Context}Context context, I${Context}ContextConfig config)
    {
${DataAssignment}
    }";
        
        private const string PER_ENTITY_CONFIG_METHOD = 
@"    public static void Configure(this ${Context}Entity entity, I${Context}EntityConfig config)
    {
${DataAssignment}
    }";

        private const string CONTEXT_CONFIGURATION = @"        contexts.${context}.Configure(config.${context});";
        
        private const string CONTEXT_DATA_ASSIGNMENT = @"        context.Replace${Component}(${FieldNames});";
        private const string CONTEXT_FLAG_ASSIGNMENT = @"        context.is${Component} = config.${component};";
        
        private const string ENTITY_DATA_ASSIGNMENT = @"        entity.Replace${Component}(${FieldNames});";
        private const string ENTITY_FLAG_ASSIGNMENT = @"        entity.is${Component} = config.${component};";
        
        private const string FIELD_NAMES = @"config.${component}.${fieldName}";
        private const string FIELD_NAMES_FLAG = @"config.${fieldName}";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
        {
            return Generate(data.OfType<ConfigData>().ToArray());
        }

        private CodeGenFile[] Generate(ConfigData[] data)
        {
            var uniqueContexts = data.Where(d => d.IsUnique()).SelectMany(src => src.GetContextNames()).Distinct().ToArray();
            var nonUniqueContexts = data.Where(d => !d.IsUnique()).SelectMany(src => src.GetContextNames()).Distinct().ToArray();
            var uniqueData = data.Where(d => d.IsUnique()).ToArray();
            var nonUniqueData = data.Where(d => !d.IsUnique()).ToArray();

            var fileContents = TEMPLATE
                .Replace("${ContextsConfigMethod}", GenerateContextsConfigMethodCalls(uniqueContexts))
                .Replace("${PerContextConfigMethods}", GeneratePerContextConfigMethods(uniqueContexts, uniqueData))
                .Replace("${PerEntityConfigMethods}", GeneratePerEntityConfigMethods(nonUniqueContexts, nonUniqueData));

            var fileName = $"ConfigGenerator{Path.DirectorySeparatorChar}Extentions{Path.DirectorySeparatorChar}ConfigExtentions.cs";
            
            return new[] { new CodeGenFile(fileName, fileContents, GetType().FullName) };
        }

        private string GenerateContextsConfigMethodCalls(string[] contexts) => CONTEXTS_CONFIG_METHOD
            .Replace("${ContextMethodCalls}",
            string.Join("\n",
                contexts.Select(ctx => CONTEXT_CONFIGURATION.Replace("${context}", ctx.LowercaseFirst()))
                    .ToArray()));

        private string GeneratePerContextConfigMethods(string[] contexts, ConfigData[] data) => string.Join("\n\n",
            contexts.Select(ctx
                    => PER_CONTEXT_CONFIG_METHOD
                        .Replace("${Context}", ctx)
                        .Replace("${DataAssignment}", GeneratePerContextDataAssignments(ctx, data)))
                .ToArray());

        private string GeneratePerContextDataAssignments(string ctx, ConfigData[] data) => string.Join("\n",
            data.Where(d => d.GetContextNames().Contains(ctx)).Select(d
                => (d.IsFlag() ? CONTEXT_FLAG_ASSIGNMENT : CONTEXT_DATA_ASSIGNMENT)
                .Replace("${Component}", d.GetTypeName().ToComponentName(false))
                .Replace("${component}", d.GetTypeName().ToComponentName(false).LowercaseFirst())
                .Replace("${FieldNames}", GetFieldNames(d))).ToArray());
        
        private static string GeneratePerEntityConfigMethods(string[] contexts, ConfigData[] data)
            => string.Join("\n\n",
                contexts.Select(ctx
                        => PER_ENTITY_CONFIG_METHOD
                            .Replace("${Context}", ctx)
                            .Replace("${DataAssignment}", GeneratePerEntityDataAssignments(ctx, data)))
                    .ToArray());
        
        private static string GeneratePerEntityDataAssignments(string ctx, ConfigData[] data) => string.Join("\n",
            data.Where(d => d.GetContextNames().Contains(ctx)).Select(d
                => (d.IsFlag() ? ENTITY_FLAG_ASSIGNMENT : ENTITY_DATA_ASSIGNMENT)
                .Replace("${Component}", d.GetTypeName().ToComponentName(false))
                .Replace("${component}", d.GetTypeName().ToComponentName(false).LowercaseFirst())
                .Replace("${FieldNames}", GetFieldNames(d))).ToArray());

        private static string GetFieldNames(ConfigData data) => string.Join(", ",
            data.GetMemberData().Select(member => (data.GetMemberData().Length > 0 ? FIELD_NAMES : FIELD_NAMES_FLAG)
                .Replace("${component}", data.GetTypeName().ToComponentName(false).LowercaseFirst())
                .Replace("${fieldName}", member.name.LowercaseFirst())).ToArray());
    }
}