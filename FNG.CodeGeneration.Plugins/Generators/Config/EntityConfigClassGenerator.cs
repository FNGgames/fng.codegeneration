﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class EntityConfigClassGenerator : ConfigGenerator
    {
        public override string name => "Entity Config (Class)";
        
        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Entity, DestinationType.Class, data);
    }
}