﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class ContextsConfigClassGenerator : ConfigGenerator
    {
        public override string name => "Contexts Config (Class)";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Contexts, DestinationType.Class, data);
    }
}