﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class ContextConfigInterfaceGenerator : ConfigGenerator
    {
        public override string name => "Context Config (Interface)";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Context, DestinationType.Interface, data);
    }
}