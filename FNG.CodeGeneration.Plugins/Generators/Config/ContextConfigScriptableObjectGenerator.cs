﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class ContextConfigScriptableObjectGenerator : ConfigGenerator
    {
        public override string name => "Context Config (Scriptable Object)";
        
        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Context, DestinationType.ScriptableObject, data);
    }
}