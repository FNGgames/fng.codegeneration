﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class ContextsConfigScriptableObjectGenerator : ConfigGenerator
    {
        public override string name => "Contexts Config (Scriptable Object)";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Contexts, DestinationType.ScriptableObject, data);
    }
}