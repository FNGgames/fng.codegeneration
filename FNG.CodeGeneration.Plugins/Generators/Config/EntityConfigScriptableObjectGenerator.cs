﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class EntityConfigScriptableObjectGenerator : ConfigGenerator
    {
        public override string name => "Entity Config (Scriptable Object)";
        
        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Entity, DestinationType.ScriptableObject, data);
    }
}