﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class ContextConfigClassGenerator : ConfigGenerator
    {
        public override string name => "Context Config (Class)";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Context, DestinationType.Class, data);
    }
}