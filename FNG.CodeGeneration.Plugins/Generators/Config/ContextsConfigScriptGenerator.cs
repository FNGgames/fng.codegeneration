﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class ContextsConfigScriptGenerator : ConfigGenerator
    {
        public override string name => "Contexts Config (Script)";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Contexts, DestinationType.Script, data);
    }
}