﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;
using FNG.CodeGeneration.Plugins;

public abstract class ConfigGenerator : AbstractGenerator
{
    private const string CONTEXTS_INTERFACE_TEMPLATE = @"public interface IContextsConfig
{
${Properties}
}";     

    
    private const string CONTEXTS_CLASS_TEMPLATE = @"
using Sirenix.OdinInspector;
using Sirenix.Serialization;

public class ContextsConfig : IContextsConfig
{
${Fields}

${Properties}
}";         
    
    private const string CONTEXTS_SCRIPTABLEOBJECT_TEMPLATE = @"using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

[CreateAssetMenu(menuName = ""Config/Contexts Config"", fileName = ""Contexts Config"", order = 0)]
public class ContextsConfigAsset : SerializedScriptableObject, IContextsConfig
{
${Fields}

${Properties}
}"; 
    
    private const string CONTEXTS_SCRIPT_TEMPLATE = @"using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

[AddComponentMenu(""Config/Contexts Config"", 0)]
public class ContextsConfigScript : SerializedMonoBehaviour, IContextsConfig
{
${Fields}

${Properties}
}";

    private const string INTERFACE_TEMPLATE = @"public interface I${Context}${Domain}Config
{
${Properties}
}"; 
    
    private const string CLASS_TEMPLATE = @"
using Sirenix.OdinInspector;
using Sirenix.Serialization;

public class ${Context}${Domain}Config : I${Context}${Domain}Config
{
${Fields}

${Properties}
}";

    private const string SCRIPT_TEMPLATE = @"
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

[AddComponentMenu(""Config/${Context} ${Domain} Config"", ${Order})]
public class ${Context}${Domain}ConfigScript : SerializedMonoBehaviour, I${Context}${Domain}Config
{
${Fields}

${Properties}
}"; 
    
    private const string SCRIPTABLEOBJECT_TEMPLATE = @"
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

[CreateAssetMenu(menuName = ""Config/${Context} ${Domain} Config"", fileName = ""${Context} ${Domain} Config"", order = ${Order})]
public class ${Context}${Domain}ConfigAsset : SerializedScriptableObject, I${Context}${Domain}Config
{
${Fields}

${Properties}
}";
    
    private const string CONTEXTS_FIELDS = @"    [OdinSerialize, Title(""${Context} Context"")]
private I${Context}ContextConfig _${context};";
    private const string CONTEXTS_INTERFACE_PROPERTIES = @"    I${Context}ContextConfig ${context} { get; }";
    private const string CONTEXTS_PROPERTIES = @"    public I${Context}ContextConfig ${context} => _${context};";
    
    private const string COMPONENT_INTERFACE_PROPERTY = @"    ${Component} ${component} { get; }";
    private const string COMPONENT_INTERFACE_PROPERTY_FLAG = @"    bool ${component} { get; }";
    
    private const string COMPONENT_FIELD = @"    [OdinSerialize, Title(""${Component}"")] 
    ${Component} _${component} = new ${Component}();";
    private const string COMPONENT_FIELD_FLAG = @"    [OdinSerialize, Title(""${Component}"")] 
    private bool _${component};";

    private const string COMPONENT_ACCESSOR_PROPERTY = @"    public ${Component} ${component} => _${component};";
    private const string COMPONENT_ACCESSOR_PROPERTY_FLAG = @"    public bool ${component} => _${component};";

    private const string ORDER_TOKEN = "${Order}";
    private const string CONTEXTS_TOKEN = "${Context}";
    private const string DOMAIN_TOKEN = "${Domain}";
    private const string COMPONENT_TYPENAME_TOKEN = "${Component}";
    private const string COMPONENT_LOWERCASEFIRST_TOKEN = "${component}";
    private const string FIELDS_TOKEN = "${Fields}";
    private const string PROPERTIES_TOKEN = "${Properties}";

    protected static CodeGenFile[] GenerateFiles(Domain domain, DestinationType type, CodeGeneratorData[] data)
    {
        var sourceData = data.OfType<ConfigData>().Where(_filters[domain]).ToArray();
        var contexts = sourceData.Where(_filters[domain]).SelectMany(d => d.GetContextNames()).Distinct().ToArray();
        
        if (domain == Domain.Contexts)
        {
            var fileName = $"{_directoryNames[type]}{domain}Config{_suffixes[type]}.cs";
            var fileContents = _contextsTemplates[type]
                .Replace(FIELDS_TOKEN, GetContextsFields(contexts))
                .Replace(PROPERTIES_TOKEN, type == DestinationType.Interface ? GetContextsInterfaceProperties(contexts) : GetContextsProperties(contexts));
            return new[] { new CodeGenFile(fileName, fileContents, $"{domain}Config{type}Generator") };
        }

        var order = domain == Domain.Context ? 100 : domain == Domain.Entity ? 1000 : 1;
        return contexts.Select(ctx => GenerateFile(domain, type, ctx, data, ref order)).ToArray();
    }
    
    protected static CodeGenFile GenerateFile(Domain domain, DestinationType type, string context, CodeGeneratorData[] cgData, ref int order)
    {
        var data = cgData.OfType<ConfigData>().Where(_filters[domain]).ToArray();
        var template = _templates[type];
        var fileName = $"{_directoryNames[type]}{context}{domain}Config{_suffixes[type]}.cs";
        var components = data.Where(d => d.GetContextNames().Contains(context)).ToArray();
        var fileContents = template
            .Replace(ORDER_TOKEN, order++.ToString())
            .Replace(DOMAIN_TOKEN, domain.ToString())
            .Replace(CONTEXTS_TOKEN, context)
            .Replace(PROPERTIES_TOKEN, type == DestinationType.Interface ? GetInterfaceProperties(components) : GetProperties(components))
            .Replace(FIELDS_TOKEN, GetFields(components));
        return new CodeGenFile(fileName, fileContents, $"{domain}Config{type}Generator");
    }
    
    private static string GetInterfaceProperties(ConfigData[] data) => string.Join("\n",
        data.Select(d => (d.IsFlag() ? COMPONENT_INTERFACE_PROPERTY_FLAG : COMPONENT_INTERFACE_PROPERTY)
                .Replace(COMPONENT_TYPENAME_TOKEN, d.GetTypeName())
                .Replace(COMPONENT_LOWERCASEFIRST_TOKEN, d.GetTypeName().ToComponentName(false).LowercaseFirst()))
            .ToArray()
    );

    private static string GetProperties(ConfigData[] data) => string.Join("\n",
        data.Select(d => (d.IsFlag() ? COMPONENT_ACCESSOR_PROPERTY_FLAG : COMPONENT_ACCESSOR_PROPERTY)
                .Replace(COMPONENT_TYPENAME_TOKEN, d.GetTypeName())
                .Replace(COMPONENT_LOWERCASEFIRST_TOKEN, d.GetTypeName().ToComponentName(false).LowercaseFirst()))
            .ToArray()
    );
    
    private static string GetFields(ConfigData[] data) => string.Join("\n",
        data.Select(d => (d.IsFlag() ? COMPONENT_FIELD_FLAG : COMPONENT_FIELD)
                .Replace(COMPONENT_TYPENAME_TOKEN, d.GetTypeName())
                .Replace(COMPONENT_LOWERCASEFIRST_TOKEN, d.GetTypeName().ToComponentName(false).LowercaseFirst()))
            .ToArray()
    );
    
    private static string GetContextsFields(string[] contexts) => string.Join("\n",
        contexts.Select(ctx => CONTEXTS_FIELDS
                .Replace("${Context}", ctx.UppercaseFirst())
                .Replace("${context}", ctx.LowercaseFirst()))
            .ToArray()
    );
    
    private static string GetContextsProperties(string[] contexts) => string.Join("\n",
        contexts.Select(ctx => CONTEXTS_PROPERTIES
                .Replace("${Context}", ctx.UppercaseFirst())
                .Replace("${context}", ctx.LowercaseFirst()))
            .ToArray()
    );
    
    private static string GetContextsInterfaceProperties(string[] contexts) => string.Join("\n",
        contexts.Select(ctx => CONTEXTS_INTERFACE_PROPERTIES
                .Replace("${Context}", ctx.UppercaseFirst())
                .Replace("${context}", ctx.LowercaseFirst()))
            .ToArray()
    );

    protected enum Domain { Contexts, Context, Entity }

    protected enum DestinationType
    {
        Interface,
        Class,
        Script,
        ScriptableObject
    }
    
    private static readonly Dictionary<DestinationType, string> _directoryNames = new Dictionary<DestinationType, string>
    {
        {DestinationType.Interface, $"ConfigGenerator{Path.DirectorySeparatorChar}Interfaces{Path.DirectorySeparatorChar}I"},
        {DestinationType.Class, $"ConfigGenerator{Path.DirectorySeparatorChar}Classes{Path.DirectorySeparatorChar}"},
        {DestinationType.Script, $"ConfigGenerator{Path.DirectorySeparatorChar}Scripts{Path.DirectorySeparatorChar}"},
        {DestinationType.ScriptableObject, $"ConfigGenerator{Path.DirectorySeparatorChar}ScriptableObjects{Path.DirectorySeparatorChar}"}
    };
    
    private static readonly Dictionary<DestinationType, string> _templates = new Dictionary<DestinationType, string>
    {
        {DestinationType.Interface, INTERFACE_TEMPLATE},
        {DestinationType.Class, CLASS_TEMPLATE},
        {DestinationType.Script, SCRIPT_TEMPLATE},
        {DestinationType.ScriptableObject, SCRIPTABLEOBJECT_TEMPLATE}
    };
    
    private static readonly Dictionary<DestinationType, string> _contextsTemplates = new Dictionary<DestinationType, string>
    {
        {DestinationType.Interface, CONTEXTS_INTERFACE_TEMPLATE},
        {DestinationType.Class, CONTEXTS_CLASS_TEMPLATE},
        {DestinationType.Script, CONTEXTS_SCRIPT_TEMPLATE},
        {DestinationType.ScriptableObject, CONTEXTS_SCRIPTABLEOBJECT_TEMPLATE}
    };
    
    private static readonly Dictionary<DestinationType, string> _suffixes = new Dictionary<DestinationType, string>
    {
        {DestinationType.Interface, ""},
        {DestinationType.Class, ""},
        {DestinationType.Script, "Script"},
        {DestinationType.ScriptableObject, "Asset"}
    };
    
    private static readonly Dictionary<Domain, Func<ConfigData, bool>> _filters = new Dictionary<Domain, Func<ConfigData, bool>>()
    {
        {Domain.Contexts, data => data.IsUnique()},
        {Domain.Context, data => data.IsUnique()},
        {Domain.Entity, data => !data.IsUnique()}
    };
    
    
}