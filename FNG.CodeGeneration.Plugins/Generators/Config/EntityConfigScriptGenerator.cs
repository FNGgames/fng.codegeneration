﻿using System.IO;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.Utils;
using Entitas.CodeGeneration.Plugins;

namespace FNG.CodeGeneration.Plugins
{
    public class EntityConfigScriptGenerator : AbstractGenerator
    {
        public override string name => "Entity Config (Script)";

        private const string TEMPLATE = @"
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

[AddComponentMenu(""Config/${Context} Entity Config"", ${Order})]
public class ${Context}EntityConfigScript : SerializedMonoBehaviour, I${Context}EntityConfig
{
${Fields}

${Properties}
}";

        private const string FIELD = @"    [OdinSerialize, Title(""{Component}"")] private ${Component} _${component} = new ${Component}();";
        private const string FLAG_FIELD = @"    [OdinSerialize, Title(""{Component}"")] private bool _${component};";

        private const string PROPERTY = @"    public ${Component} ${component} => _${component};";
        private const string FLAG_PROPERTY = @"    public bool ${component} => _${component};";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
        {
            var configData = data.OfType<ConfigData>().Where(d => !d.IsUnique()).ToArray();
            var contexts = configData.SelectMany(src => src.GetContextNames()).Distinct();
            var order = 1;
            return contexts.Select(ctx => generateFile(ctx, configData, ref order)).ToArray();
        }

        private CodeGenFile generateFile(string context, ConfigData[] data, ref int order)
        {
            var fileName = $"Config{Path.DirectorySeparatorChar}Scripts{Path.DirectorySeparatorChar}{context}EntityConfigScript.cs";
            var components = data.Where(d => d.GetContextNames().Contains(context)).ToArray();
            var fileContents = TEMPLATE
                .Replace("${Order}", order++.ToString())
                .Replace("${Context}", context)
                .Replace("${Fields}", getFields(components))
                .Replace("${Properties}", getProperties(components));
            return new CodeGenFile(fileName, fileContents, GetType().FullName);
        }

        private string getFields(ConfigData[] data) => string.Join("\n",
            data.Select(d => (d.IsFlag() ? FLAG_FIELD : FIELD)
                .Replace("${Component}", d.GetTypeName())
                .Replace("${component}", d.GetTypeName().ToComponentName(false).LowercaseFirst()))
                .ToArray()
            );

        private string getProperties(ConfigData[] data) => string.Join("\n",
            data.Select(d => (d.IsFlag() ? FLAG_PROPERTY : PROPERTY)
                .Replace("${Component}", d.GetTypeName())
                .Replace("${component}", d.GetTypeName().ToComponentName(false).LowercaseFirst()))
                .ToArray()
            );
    }
}