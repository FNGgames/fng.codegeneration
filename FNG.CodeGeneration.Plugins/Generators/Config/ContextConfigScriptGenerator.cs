﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class ContextConfigScriptGenerator : ConfigGenerator
    {
        public override string name => "Context Config (Script)";
        
        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Context, DestinationType.Script, data);
        
    }
}