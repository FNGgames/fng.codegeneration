﻿using DesperateDevs.CodeGeneration;

namespace FNG.CodeGeneration.Plugins
{
    public class ContextsConfigInterfaceGenerator : ConfigGenerator
    {
        public override string name => "Contexts Config (Interface)";

        public override CodeGenFile[] Generate(CodeGeneratorData[] data)
            => GenerateFiles(Domain.Contexts, DestinationType.Interface, data);
    }
}