﻿using System.Collections.Generic;
using System.Linq;
using DesperateDevs.CodeGeneration;
using DesperateDevs.CodeGeneration.CodeGenerator;
using DesperateDevs.CodeGeneration.Plugins;
using DesperateDevs.Roslyn;
using DesperateDevs.Roslyn.CodeGeneration.Plugins;
using DesperateDevs.Serialization;
using DesperateDevs.Utils;
using FNG.CodeGeneration.Attributes;
using Microsoft.CodeAnalysis;

namespace FNG.CodeGeneration.Plugins
{
    public class CollectionDataProvider : IDataProvider, IConfigurable, ICachable
    {
        public string name => "Collection";
        public int priority => 1;
        public bool runInDryMode => true;
        public Dictionary<string, object> objectCache { get; set; }

        public Dictionary<string, string> defaultProperties
            => _projectPathConfig.defaultProperties.Merge(_dataProviders.OfType<IConfigurable>()
                .Select(i => i.defaultProperties).ToArray());

        private readonly CodeGeneratorConfig _codeGeneratorConfig = new CodeGeneratorConfig();
        private readonly ProjectPathConfig _projectPathConfig = new ProjectPathConfig();
        private readonly ICustomDataProvider[] _dataProviders;

        public CollectionDataProvider()
        {
            _dataProviders = new ICustomDataProvider[]
            {
                new ComponentTypeDataProvider(),
                new ComponentContextsDataProvider(),
                new MemberDataProvider(),
            };
        }

        public void Configure(Preferences preferences)
        {
            _projectPathConfig.Configure(preferences);
            _codeGeneratorConfig.Configure(preferences);

            foreach (var dataProvider in _dataProviders.OfType<IConfigurable>())
                dataProvider.Configure(preferences);
        }

        public CodeGeneratorData[] GetData()
        {
            return PluginUtil.GetCachedProjectParser(objectCache, _projectPathConfig.projectPath).GetTypes()
                .Where(t => t.GetAttribute<CollectionAttribute>() != null && t.GetPublicMembers(false).Length == 1)
                .Select(CreateData).ToArray();
        }

        private CollectionData CreateData(INamedTypeSymbol type)
        {
            var data = new CollectionData();
            foreach (var provider in _dataProviders) provider.Provide(type, data);
            return data;
        }
    }
}