﻿using DesperateDevs.Roslyn;
using Microsoft.CodeAnalysis;

namespace FNG.CodeGeneration.Plugins
{
    public class ComponentTypeDataProvider : ICustomDataProvider
    {
        public void Provide(INamedTypeSymbol type, BaseData data)
        {
            data.SetTypeName(type.ToCompilableString());
        }
    }

    public static class ComponentTypeDataExtension
    {
        public const string KEY = "Config.TypeName";

        public static string GetTypeName(this BaseData data) => (string) data[KEY];

        public static void SetTypeName(this BaseData data, string fullTypeName)
        {
            data[KEY] = fullTypeName;
        }
    }
}