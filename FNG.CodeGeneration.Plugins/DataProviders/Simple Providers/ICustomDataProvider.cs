﻿using Microsoft.CodeAnalysis;

namespace FNG.CodeGeneration.Plugins
{
    public interface ICustomDataProvider
    {
        void Provide(INamedTypeSymbol type, BaseData data);
    }
}