﻿using System.Linq;
using DesperateDevs.Roslyn;
using Entitas.CodeGeneration.Attributes;
using Microsoft.CodeAnalysis;

namespace FNG.CodeGeneration.Plugins
{
    public class ComponentIsFlagDataProvider : ICustomDataProvider
    {
        public void Provide(INamedTypeSymbol type, BaseData data)
        {
            var isFlag = !type.GetPublicMembers(false).OfType<IFieldSymbol>().Any();
            data.IsFlag(isFlag);
        }
    }

    public static class ComponentIsFlagDataExtention
    {
        private const string KEY = "Config.IsFlag";

        public static bool IsFlag(this BaseData data) => (bool) data[KEY];

        public static void IsFlag(this BaseData data, bool value)
        {
            data[KEY] = value;
        }
    }
}