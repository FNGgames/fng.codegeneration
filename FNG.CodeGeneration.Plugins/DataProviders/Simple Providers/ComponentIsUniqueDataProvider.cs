﻿using DesperateDevs.Roslyn;
using Entitas.CodeGeneration.Attributes;
using Microsoft.CodeAnalysis;

namespace FNG.CodeGeneration.Plugins
{
    public class ComponentIsUniqueDataProvider : ICustomDataProvider
    {
        public void Provide(INamedTypeSymbol type, BaseData data)
        {
            data.IsUnique(type.GetAttribute<UniqueAttribute>() != null);
        }
    }

    public static class ComponentIsUniqueDataExtention
    {
        private const string KEY = "Config.IsUnique";

        public static bool IsUnique(this BaseData data) => (bool) data[KEY];

        public static void IsUnique(this BaseData data, bool value)
        {
            data[KEY] = value;
        }
    }
}