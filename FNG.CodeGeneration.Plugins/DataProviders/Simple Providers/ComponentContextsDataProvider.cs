﻿using System.Collections.Generic;
using DesperateDevs.Serialization;
using Entitas.CodeGeneration.Plugins;
using Microsoft.CodeAnalysis;
using ContextsComponentDataProvider = Entitas.Roslyn.CodeGeneration.Plugins.ContextsComponentDataProvider;

namespace FNG.CodeGeneration.Plugins
{
    public class ComponentContextsDataProvider : ICustomDataProvider, IConfigurable
    {
        public Dictionary<string, string> defaultProperties => _contextNamesConfig.defaultProperties;
        private readonly ContextsComponentDataProvider _dp = new ContextsComponentDataProvider();
        private readonly ContextNamesConfig _contextNamesConfig = new ContextNamesConfig();

        public void Configure(Preferences preferences)
        {
            _contextNamesConfig.Configure(preferences);
            _dp.Configure(preferences);
        }

        public void Provide(INamedTypeSymbol type, BaseData data)
        {
            var dummyData = new ComponentData();
            _dp.Provide(type, dummyData);
            data.SetContextNames(dummyData.GetContextNames());
        }
    }

    public static class ComponentContextsDataExtension
    {
        public const string KEY = "Config.ContextNames";

        public static string[] GetContextNames(this BaseData data) => (string[]) data[KEY];

        public static void SetContextNames(this BaseData data, string[] contextNames)
        {
            data[KEY] = contextNames;
        }
    }
}