﻿using System.Linq;
using DesperateDevs.Roslyn;
using Entitas.CodeGeneration.Plugins;
using Microsoft.CodeAnalysis;

namespace FNG.CodeGeneration.Plugins
{
    public class MemberDataProvider : ICustomDataProvider
    {
        public void Provide(INamedTypeSymbol type, BaseData data) => data.SetMemberData(type.GetPublicMembers(false)
            .OfType<IFieldSymbol>().Select(field => new MemberData(field.Type.ToCompilableString(), field.Name))
            .ToArray());
    }

    public static class MemberDataExtension
    {
        public const string KEY = "Config.MemberData";

        public static MemberData[] GetMemberData(this BaseData data) => (MemberData[]) data[KEY];

        public static void SetMemberData(this BaseData data, MemberData[] memberData)
        {
            data[KEY] = memberData;
        }
    }
}