Released under MIT License

Copyright (c) 2018 Spiral Circus Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-----------------------------------

Custom code generators for entitas

[Contents]

1. Collection Helper Generator

Usage:
[Game, Collection]
public sealed class TestComponent : IComponent
{
    // Component must have exactly one public field
    // Field type must implement ICollection<T>
    public HashSet<int> values;
}

Generates:
public static class GameTestCollectionHelpers
{
    public static void AddTestItem(this GameEntity e, byte item)
    {
        if (!e.hasTest) e.AddTest(new System.Collections.Generic.HashSet<int>() {item});
        else if (!e.test.values.Contains(item))
        {
            e.test.values.Add(item);
            e.ReplaceTest(e.test.values);
        }
    }

    public static void RemoveTestItem(this GameEntity e, byte item)
    {
        if (!e.hasTest) return;
        e.test.values.Remove(item);
        if (e.test.values.Count == 0) e.RemoveTest();
        else e.ReplaceTest(e.test.values);
    }

    public static void ClearTestCollection(this GameEntity e)
    {
        if (!e.hasTest) return;
        e.test.values.Clear();
        e.ReplaceTest(e.test.values);
    }
    
    public static HashSet<int> GetOrCreateTestCollection(this GameEntity e)
        => e.hasTest ? e.test.values : new HashSet<int>();

    public static bool HasTestItem(this GameEntity e, byte item)
        => e.hasTest && e.test.values.Contains(item);
}




2. Enum Helper Generator

usage:
[Game, Enum]
public sealed class TestComponent : IComponent
{
    // Component must have exactly one public field
    // Field type must be equatable 
    // i.e. (a == b) must compile and give sensible results
    public SomeEnum value;
}

generates:
public static class GameTestEnumHelpers
{
    public static bool IsTest(this GameEntity e, SomeEnum value)
        => e.hasTest && e.test.value == value;
}



3. Creation Helper Generator

usage:
[Game, Creation
public sealed class TestComponent : IComponent
{
    // flag components also supported
    public int intValue;
    public float floatValue;
    public string stringValue;
}

generates:
public static class GameTestCreationHelpers
{
    public static GameEntity CreateTest(this GameContext c, int intValue, float floatValue, string stringValue)
    {
        var e = c.CreateEntity();
        e.AddTest(intValue, floatValue, stringValue;
        return e;
    }
}

This is a context extention instead of an entity extention. Useful for "fire and forget" 
components, e.g. commands, collisions etc.




4. Reactive Event System Generator

This one uses the same data as the standard event system generator but 
generates reactive systems that react on the event LISTENER being added 
instead of the component that the listener listens to.

The point is to have the event called as soon as the listener is attached 
if the entity has the appropriate component (or doesn't have it in the 
case of .Removed events).

