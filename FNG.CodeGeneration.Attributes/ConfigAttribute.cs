﻿using System;

namespace FNG.CodeGeneration.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ConfigAttribute : Attribute
    {
    }
}